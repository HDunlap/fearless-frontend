function stateOption (name, abbreviation) {
    return `
    <option selected value="${abbreviation}">${name}</option>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/'
    try {
        const response = await fetch(url)

        if (!response.ok) {
            throw new Error('response not ok')
        } else {
            const data = await response.json()

            for (let state of data.states) {
                const name = state.name
                const abbreviation = state.abbreviation
                const html = stateOption(name, abbreviation)
                const menu = document.querySelector("#state")
                menu.innerHTML += html
            }

            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData))

                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                }
            });
        }
    } catch (e) {
        throw new Error('Failed to fetch response')
    }

})
