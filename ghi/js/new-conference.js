function locationOption (name, id) {
    return `
    <option selected value="${id}">${name}</option>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/'
    try {
        const response = await fetch(url)

        if (!response.ok) {
            throw new Error('response not ok')
        } else {
            const data = await response.json()

            for (let location of data.locations) {
                const name = location.name
                const id = location.id
                const html = locationOption(name, id)
                const menu = document.querySelector("#location")
                menu.innerHTML += html
            }

            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData))

                const conferenceUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                }
            });
        }
    } catch (e) {
        throw new Error('Failed to fetch response')
    }

})
