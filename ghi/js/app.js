function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
    <div class="col mb-3">
      <div class="card shadow bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body p-3">
          <h5 class="card-title">${name}</h5>
          <p class="text-muted">${location}
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
            <p class="card-text"><small>${starts} - ${ends}</small></p>
          </div>
      </div>
    </div
    `;
  }

  function errorCard(e) {
    return `
    <div class="alert alert-danger" role="alert">
        An error occured: ${e}
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'
    try {
        const response = await fetch(url)

        if (!response.ok) {
            throw new Error('response not ok')
        } else {
            const data = await response.json()

            for (let conference of data.conferences){

                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {

                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts);
                    const startsFormatted = starts.toLocaleDateString()
                    const ends = new Date(details.conference.ends);
                    const endsFormatted = ends.toLocaleDateString()
                    const location = details.conference.location.name
                    const html = createCard(name, description, pictureUrl, location, startsFormatted, endsFormatted);
                    const column = document.querySelector('.row')
                    column.innerHTML += html
                }
            }
        }
    } catch (e) {
        const er = 'could not load conferences'
        const html = errorCard(er)
        const error = document.querySelector('.error-zone')
        error.innerHTML += html
    }

});
