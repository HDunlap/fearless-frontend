import React from 'react';
import { useEffect, useState } from "react"

function ConferenceForm () {
    const [locations, setLocations] = useState([])

    const [formData, setFormData] = useState({
        name: "",
        starts: "",
        ends: "",
        description: "",
        max_presentations: "",
        max_attendees: "",
        location: "",
    })

    const handleFieldChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async (e) => {
      e.preventDefault()
      const data = formData

      console.log("data: ", data)

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
          'Content-Type': 'application/json',
          },
      };

      const response = await fetch(conferenceUrl, fetchConfig);
      console.log(response)
      if (response.ok) {
          const newConference = await response.json();
          console.log("New conference: ", newConference)

          setFormData({
            name: "",
            starts: "",
            ends: "",
            description: "",
            max_presentations: "",
            max_attendees: "",
            location: ""
          })
      }
  }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);




    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.starts} placeholder="mm/dd/yyy" required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.ends} placeholder="mm/dd/yyy" required type="date" id="ends" name="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="name">Description</label>
                <textarea onChange={handleFieldChange} value={formData.description} className="form-control" id="description" name="description" rows="3"></textarea>

              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.max_presentations} placeholder="Maximum presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.max_attendees} placeholder="Maximum attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFieldChange} value={formData.location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (<option key={location.id} value={location.id}>{location.name}</option>)
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm
