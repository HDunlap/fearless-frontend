import { useEffect, useState } from "react"


function AttendForm () {
  const [conferences, setConferences] = useState([])

  const [formData, setFormData] = useState({
      name: "",
      email: "",
      import_href: ""
  })

  const handleFieldChange = (e) => {
      setFormData({
          ...formData,
          [e.target.name]: e.target.value
      })
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    const data = {
      name: formData.name,
      email: formData.email
    }

    console.log("data: ", data)

    const attendeeUrl = `http://localhost:8001${formData.import_href}attendees/`;
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };

    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
        const newAttendee = await response.json();
        console.log("New attendee: ", newAttendee)
        setFormData({
          name: "",
          email: "",
          import_href: ""
        })
    }
  }

  const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences)
        const selectTag = document.getElementById('conference')
        const loadIcon = document.getElementById('loading-conference-spinner')
        loadIcon.classList.add("d-none")
        selectTag.classList.remove("d-none")
      }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt=""/>
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleFieldChange} value={formData.input_href} name="import_href" id="conference" className="form-select d-none" required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                      return (
                        <option value={conference.href} key={conference.href}>{conference.name}</option>
                      )
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleFieldChange} value={formData.name} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleFieldChange} value={formData.email} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AttendForm
