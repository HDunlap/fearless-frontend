import { useState, useEffect } from "react"

function PresentationForm () {
  const [conferences, setConferences] = useState([])

  const [formData, setFormData] = useState({
      presenter_name: "",
      presenter_email: "",
      company_name: "",
      title: "",
      synopsis: "",
      import_href: ""
  })

  const handleFieldChange = (e) => {
      setFormData({
          ...formData,
          [e.target.name]: e.target.value
      })
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    const data = {
      presenter_name: formData.presenter_name,
      presenter_email: formData.presenter_email,
      company_name: formData.company_name,
      title: formData.title,
      synopsis: formData.synopsis
    }

    console.log("data: ", data)

    const attendeeUrl = `http://localhost:8000${formData.import_href}presentations/`;
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };

    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
        const newPresentation = await response.json();
        console.log("New presentation: ", newPresentation)

        setFormData({
          presenter_name: "",
          presenter_email: "",
          company_name: "",
          title: "",
          synopsis: "",
          import_href: ""
        })
    }
  }

  const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences)
      }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.presenter_name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.presenter_email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.company_name} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFieldChange} value={formData.title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleFieldChange} value={formData.synopsis} id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleFieldChange} value={formData.import_href} required name="import_href" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                      <option value={conference.href} key={conference.href}>{conference.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}


export default PresentationForm
